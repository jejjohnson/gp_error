from sklearn.externals.joblib import Parallel, delayed
import numpy as np


def generate_batches(n_samples, batch_size):
    """A generator to split an array of 0 to n_samples
    into an array of batch_size each.

    Parameters
    ----------
    n_samples : int
        the number of samples

    batch_size : int,
        the size of each batch


    Returns
    -------
    start_index, end_index : int, int
        the start and end indices for the batch

    Source:
        https://github.com/scikit-learn/scikit-learn/blob/master
        /sklearn/utils/__init__.py#L374
    """
    start_index = 0

    # calculate number of batches
    n_batches = int(n_samples // batch_size)

    for _ in range(n_batches):

        # calculate the end coordinate
        end_index = start_index + batch_size

        # yield the start and end coordinate for batch
        yield start_index, end_index

        # start index becomes new end index
        start_index = end_index

    # special case at the end of the segment
    if start_index < n_samples:

        # yield the remaining indices
        yield start_index, n_samples


def gperror_parallel(model, data, sigma, n_jobs=2, verbose=1, batch_size=20):
    results = Parallel(n_jobs=n_jobs, verbose=verbose)(
        delayed(gperror_predict)(
            model, data[start:end], sigma)
        for (start, end) in generate_batches(data.shape[0], batch_size=batch_size)
    )
    # Aggregate results
    mean, variance = tuple(zip(*results))
    mean = np.hstack(mean)
    variance = np.hstack(variance)

    return mean, variance


def gperror_predict(model, x, sigma):
    n_samples = len(x)
    mean, variance = np.zeros(n_samples), np.zeros(n_samples)

    for i in range(n_samples):
        mean[i], variance[i] = model(x[i], sigma)

    return mean, variance


