# The Objective

* We need to take into account the input noise within the RS-EarthSci-ML community
    * Measurements have noise
    * Model Outputs have noise
* The Noise needs to be characterized (theory with upper bounds)
* Need Effective and Scalable methods to apply to large scale datasets
* Need to be generalizeable to other kernels (not just the RBF and ARD)


---
# My Contributions

1. Took a look at the NIGP framework
2. Disected it and got our own approximation
1. Statistical Analysis of the error in relation to the variance estimate 
4. Applied it to an EarthSci/RS setting

---
# Looking Forward

* Sparse Online Methods (SONIG)
* DL Architecture (Automatic Gradients)
* Deep GPs
* Variational Inference

