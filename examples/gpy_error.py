#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  5 17:47:09 2018

@author: eman
"""
import warnings
warnings.simplefilter('ignore')
import matplotlib.pyplot as plt
import numpy as np
import GPy
from GPy.kern import RBF, White
from GPy.util.initialization import initialize_latent
from GPy.models.bayesian_gplvm import BayesianGPLVM
from GPy.models import (GPRegression, GPHeteroscedasticRegression,
                        SparseGPRegression)
from gp_error.data import example_1d
from sklearn.gaussian_process.kernels import (RBF as ARD,
                                              WhiteKernel,
                                              ConstantKernel as C)
from sklearn.gaussian_process import GaussianProcessRegressor

# Import Data
x_error=0.3
Xdata, ydata, params = example_1d(func=1, x_error=x_error)

xtrain, ytrain = Xdata['train'], ydata['train']
xtest, ytest = Xdata['test'], ydata['test']
xplot, yplot = Xdata['plot'], ydata['plot']
print(xtrain.min(), xtrain.max())
print(xtest.min(), xtest.max())
print(xplot.min(), xplot.max())

fig, ax = plt.subplots()
ax.scatter(xtrain, ytrain)
plt.show()

x_error = params['x']
y_error = params['y']

# Kernel Matrix
input_dim = 1
lengthscale = 1.0
signal_variance = 1.0
noise_likelihood = 0.01
kernel = GPy.kern.RBF(input_dim=input_dim,
                      variance=signal_variance,
                      lengthscale=lengthscale)

#
#X, fracs = initialize_latent(init, input_dim=input_dim, Y=ytrain)
#X_variance = np.random.uniform(0, .1, X.shape)

#%%
# =============================================================================
# GP Regression Standard
# =============================================================================
gp_model = GPRegression(xtrain, ytrain, kernel=kernel)
gp_model.plot([-10, 10])
print(gp_model)

gp_model.optimize()
gp_model.plot([-10, 10])
print(gp_model)

#%%
# =============================================================================
# Sparse GP Regression Standard
# =============================================================================
sgp_model = SparseGPRegression(xtrain, ytrain)
print(sgp_model.plot())
sgp_model.optimize()
sgp_model.plot([-10, 10])
print(sgp_model)

#%%
# =============================================================================
# GP Regression Heteroscedastic
# =============================================================================
gph_model = GPHeteroscedasticRegression(xtrain, ytrain)
gph_model.optimize()
gph_model.plot([-10, 10])
print(gph_model)



#%%
# =============================================================================
# GP-LVM with no prior assumptions
# =============================================================================

#kernel.plot()

init ='Random'
input_dim = 1

X_mean, fracs = initialize_latent(init, input_dim=input_dim, Y=ytrain)
print(X_mean.min(), X_mean.max())
X_variance = np.random.uniform(0, x_error, X_mean.shape)
X_variance = np.random.normal(x_error, X_mean.shape)
gp_model = GPy.models.bayesian_gplvm.BayesianGPLVM(X=X_mean, Y=ytrain,
                                                   input_dim=input_dim,
                                                   kernel=kernel)

gp_model.likelihood.variance = 0.01
#gp_model.optimize(messages=1, max_iters=1000)

# Optimize the Model
gp_model.optimize(messages=1, max_iters=5e3)

# Show the Model Parameters
print(gp_model)

#%%




#print(gp_model.latent_space.variance)

gp_model.plot([-10, 10])
# Plot the Current Data

# Kernel Matrix
input_dim = 1
lengthscale = 0.97
signal_variance = 0.6
noise_likelihood = 0.003
kernel = GPy.kern.RBF(input_dim=input_dim,
                      variance=signal_variance,
                      lengthscale=lengthscale)
gp_model = GPRegression(xtrain, ytrain, kernel=kernel)
gp_model.plot([-20, 20])


# New Parameters
n_length_scale = bgplvm_model.rbf.variance
print('Length_scale:', n_length_scale)
#n_gp_model =
#%%
# =============================================================================
# GP-LVM with some prior assumptions
# =============================================================================

# Kernel Matrix
input_dim = 1
lengthscale = 1.0
signal_variance = 1.0
noise_likelihood = 0.01
kernel = GPy.kern.RBF(input_dim=input_dim,
                      variance=signal_variance,
                      lengthscale=lengthscale)

#X_mean = xtrain                                 # Mean X
X_variance = np.random.normal(loc=0.0, scale=x_error, size=xtrain.shape)


bgplvm_model = BayesianGPLVM(
        X_variance=X_variance,
        Y=ytrain,
        Z=xtrain,
        input_dim=input_dim,
        kernel=kernel)

bgplvm_model.likelihood.variance = 01.0

# Print the Model
print(bgplvm_model)


# Optimize the Model
bgplvm_model.optimize(messages=2, max_iters=5e5)


# Show the Model Parameters
print(bgplvm_model)

#%%




#%% GP Model with Scikit-Learn

sklearn_kernel = C(0.6, 'fixed') * ARD(0.97, 'fixed') + WhiteKernel(0.003
                  , 'fixed')
sklearn_gp_model = GaussianProcessRegressor(kernel=sklearn_kernel)
sklearn_gp_model.fit(xtrain, ytrain)
mu, std = sklearn_gp_model.predict(xplot, return_std=True)

fig, ax = plt.subplots()


ax.fill_between(
        xplot.squeeze(), 
        mu.squeeze() + std.squeeze(),
        mu.squeeze() - std.squeeze(), color='orange', alpha=.3)
ax.scatter(xtrain, ytrain)
ax.plot(xplot, mu, color='k')
plt.show()

#%% New GP Model

# Kernel Matrix
input_dim = 1
lengthscale = 0.97
signal_variance = 0.6
noise_likelihood = 0.003
kernel = GPy.kern.RBF(input_dim=input_dim,
                      variance=signal_variance,
                      lengthscale=lengthscale)
gp_model = GPRegression(xtrain, ytrain, kernel=kernel)
gp_model.plot([-20, 20])


# New Parameters
n_length_scale = bgplvm_model.rbf.variance
print('Length_scale:', n_length_scale)
#n_gp_model =

#%%
#%% New GP Model

# Kernel Matrix
input_dim = 1
lengthscale = 17.25
signal_variance = 1.2e-6
noise_likelihood = 0.645
kernel = GPy.kern.RBF(input_dim=input_dim,
                      variance=signal_variance,
                      lengthscale=lengthscale)
gp_model = GPRegression(xtrain, ytrain, kernel=kernel)
gp_model.plot([-10, 10])


# New Parameters
n_length_scale = bgplvm_model.rbf.variance
print('Length_scale:', n_length_scale)
#n_gp_model =
